FROM ubuntu:focal

# Update system
RUN apt-get update -y && apt-get upgrade -y

# Install necessary packages for MXE + install Wine and NSIS
# python-mako and python3-mako fix an issue with building cmake -> mesa (https://github.com/mxe/mxe/issues/2561)
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y autoconf automake autopoint bash bison bzip2 flex g++ g++-multilib gettext git gperf intltool libc6-dev-i386 libgdk-pixbuf2.0-dev libltdl-dev libssl-dev libtool-bin libxml-parser-perl lzip make openssl p7zip-full patch perl pkg-config python ruby sed unzip wget xz-utils \
                                                        python-mako python3-mako \
                                                        wine \
                                                        nsis
# Get MXE
RUN git clone https://github.com/mxe/mxe.git && mv mxe /opt/mxe
WORKDIR /opt/mxe

# Make CMake and Qt5 (only x86_64)
RUN make MXE_TARGETS='x86_64-w64-mingw32.shared' cmake qtbase qtgraphicaleffects qtimageformats qtnetworkauth qtquickcontrols2 qtscript qtsvg qttools qttranslations qtwinextras --jobs=6 JOBS=3 && \
    rm -rf .git .src .ccache .pkg

# Add MXE bin to the path
ENV PATH=/opt/mxe/usr/bin:$PATH
